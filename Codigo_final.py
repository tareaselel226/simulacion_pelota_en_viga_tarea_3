import RPi.GPIO as GPIO 
import time

ON = 16    # N° DE PIN ACTIVACIÓN
ECO =  18  # N° DE PIN

Velocidad_sonido = 34300 # [cm/s]
error=float(0) #inicializar variable
Derror=float(0) #inicializar variable
Ierror=float(0) #inicializar variable
centroviga= float(20)#Centro del largo donde se mueve la pelota
kp=float(0.1) #Constante proporcional # 0.35
ki=float(0.001) #Constante integrativa #  0.02
kd=float(0.01) #Constante derivativa # 0.05
infsensor= 0# Limite inferior de lectura del sensor
supsensor= 40# Limite superior de lectura del sensor
infservo= 0# Limite inferior de lectura del servo
supservo= 90# Limite superior de lectura del servo

def Medición_sensor ():
    GPIO.setup(ON,GPIO.OUT)    # PIN DE SALIDA 
    GPIO.setup(ECO,GPIO.IN)    # PIN DE ENTRADA
    GPIO.output(ON, False)      # PIN DE ACTIVACIÓN APAGADO
    time.sleep(0.2)#0.5
    GPIO.output(ON, True)       # SE ACTIVA LA SEÑAL DEL CONTROLADOR 
    time.sleep(0.000001)        # TIEMPO DE ACTIVACIÓN
    GPIO.output(ON, False)      # APAGAMOS LA SEÑAL DEL CONTROLADOR
    while GPIO.input(ECO) == 0: 
        tiempo_inicio = time.time()
    while GPIO.input(ECO) == 1: 
        tiempo_termino = time.time()
    duracion_pulso = tiempo_termino - tiempo_inicio  
    distancia = Velocidad_sonido * (duracion_pulso)/2
    distancia = round (distancia,2)   # REDONDEAR A 2 DÉCIMALES
    print ("Distancia:",distancia, "cm")
    #GPIO.cleanup() # (Sale en varios codigos de referencia, pero trae problemas)
    return (distancia)

def translate2(value, leftMin, leftMax, rightMin, rightMax):
    value=(value+20)*((rightMax-rightMin)/(leftMax-leftMin)) #20=dif max
    return(value)

def PID(Ierror,Derror,kp,ki,kd):
    dis=Medición_sensor()
    error = centroviga - dis
    if (abs(error) < 1): #Para detener la pelota
        error = 0
    if (abs(Derror) < 1): #Para detener la pelota
        Derror = 0
    if (abs(Ierror) < 1): #Para detener la pelota
        Ierror = 0
    P=float(error*kp)
    I=float(Ierror*ki)
    D=float((error-Derror)*kd)
    PID=float(P+D+I)
    Ierror += error
    Derror = error
    disPID=float(PID) #Factor PID #int
    print("disPID",disPID)
    Angulo=translate2(disPID,infsensor,supsensor,infservo,supservo)
    if (Angulo < infservo):
        Angulo = infservo
    if (Angulo > supservo):
        Angulo = supservo
    print("anguloPID",Angulo) 
    return(Angulo)

def ang(Angulo):
    pinservo = 11 #pin servo por alguna razon no funciona si se define fuera.
    GPIO.setup(pinservo,GPIO.OUT)
    servo1 = GPIO.PWM(pinservo,50) # pin 11 for servo1, pulse 50Hz
    servo1.start(0)
    angle = float(Angulo)
    servo1.ChangeDutyCycle(2+(angle/18))
    time.sleep(0.2) #0.5 valor original
    servo1.ChangeDutyCycle(0)
    servo1.stop()
    #GPIO.cleanup() # (Sale en varios codigos de referencia, pero trae problemas)

c=0
while (c<1000): #Contador para evitar que el sistema opere infinitamente
    c+=1
    b=PID(Ierror,Derror,kp,ki,kd)
    ang(b)